package nafos.server.util;

import java.io.Serializable;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @ClassName ObjWithLock
 * @Description TODO
 * @Author hxy
 * @Date 2020/3/16 16:44
 */
public class ObjWithLock<T> implements Serializable {
    private T obj;
    private ReentrantReadWriteLock lock;
    private static final long serialVersionUID = -3048283373239453901L;

    public ObjWithLock(T obj) {
        this(obj, new ReentrantReadWriteLock());
    }

    public ObjWithLock(T obj, ReentrantReadWriteLock lock) {
        this.obj = null;
        this.lock = null;
        this.obj = obj;
        this.lock = lock;
    }

    public ReentrantReadWriteLock getLock() {
        return this.lock;
    }

    public T getObj() {
        return this.obj;
    }

    public void setObj(T obj) {
        this.obj = obj;
    }
}
