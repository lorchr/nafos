package nafos.server.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @ClassName ListWithLock
 * @Description TODO
 * @Author hxy
 * @Date 2020/3/16 16:45
 */
public class ListWithLock<T> extends ObjWithLock<List<T>> {
    private static final long serialVersionUID = 8549668315606224029L;
    private static final Logger log = LoggerFactory.getLogger(ListWithLock.class);

    public ListWithLock(List<T> list) {
        super(list);
    }

    public ListWithLock(List<T> list, ReentrantReadWriteLock lock) {
        super(list, lock);
    }

    public boolean add(T t) {
        ReentrantReadWriteLock.WriteLock writeLock = this.getLock().writeLock();
        writeLock.lock();

        try {
            List<T> list = (List)this.getObj();
            boolean var4 = list.add(t);
            return var4;
        } catch (Throwable var8) {
            log.error(var8.getMessage(), var8);
        } finally {
            writeLock.unlock();
        }

        return false;
    }

    public void clear() {
        ReentrantReadWriteLock.WriteLock writeLock = this.getLock().writeLock();
        writeLock.lock();

        try {
            List<T> list = (List)this.getObj();
            list.clear();
        } catch (Throwable var6) {
            log.error(var6.getMessage(), var6);
        } finally {
            writeLock.unlock();
        }

    }

    public boolean remove(T t) {
        ReentrantReadWriteLock.WriteLock writeLock = this.getLock().writeLock();
        writeLock.lock();

        try {
            List<T> list = (List)this.getObj();
            boolean var4 = list.remove(t);
            return var4;
        } catch (Throwable var8) {
            log.error(var8.getMessage(), var8);
        } finally {
            writeLock.unlock();
        }

        return false;
    }

    public int size() {
        ReentrantReadWriteLock.ReadLock readLock = this.getLock().readLock();
        readLock.lock();

        int var3;
        try {
            List<T> list = (List)this.getObj();
            var3 = list.size();
        } finally {
            readLock.unlock();
        }

        return var3;
    }
}
