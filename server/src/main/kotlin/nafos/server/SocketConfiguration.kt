package nafos.server

/**
 * @Description socket服务启动配置
 * @Author      xinyu.huang
 * @Time        2019/11/24 1:30
 */
open class SocketConfiguration(
        /**
         * Socket启动端口号
         */
        port: Int = 9090,
        /**
         * 心跳事件 - 秒为计时单位
         */
        open var heartTimeout: Long = 5,
        /**
         * 未认证的channel多久T出去 秒为计时单位
         */
        open var unsafeTimeOut:Long = 120

) : Configuration(socketPort = port)