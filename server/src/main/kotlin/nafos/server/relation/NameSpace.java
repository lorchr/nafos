package nafos.server.relation;

import nafos.server.util.SetWithLock;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * @author 作者 huangxinyu
 * @version 创建时间：2018年3月8日 下午6:05:07
 * 类说明
 */
public class NameSpace {

    public static SetWithLock<Client> getClients(String nameSpace) {
        return IoCache.spaceClientMap.get(nameSpace);
    }


    public static void addClient(String nameSpace, Client client) {
        if (IoCache.spaceClientMap.get(nameSpace) != null) {
            IoCache.spaceClientMap.get(nameSpace).add(client);
        } else {
            HashSet<Client> set = new HashSet();
            set.add(client);
            IoCache.spaceClientMap.put(nameSpace, new SetWithLock(set));
        }
    }

    public static void removeClient(String nameSpace, Client client) {
        if (getClients(nameSpace) != null) {
            IoCache.spaceClientMap.get(nameSpace).remove(client);
        }
    }


    public static void sendMsg(String nameSpace, int id, Object obj) {
        SetWithLock<Client> clients = getClients(nameSpace);
        if (clients == null) {
            return;
        }
        for (Client client : clients.getObj()) {
            client.sendMsg(id, obj);
        }
    }

    public static SetWithLock<String> getRoomIds(String nameSpace) {
        return IoCache.spaceRoomMap.get(nameSpace);
    }

    /**
     * 创建房间
     *
     * @param nameSpace
     * @param roomId
     */
    public static void inviteRoom(String nameSpace, String roomId) {
        if (getRoomIds(nameSpace) != null) {
            IoCache.spaceRoomMap.get(nameSpace).add(roomId);
        } else {
            HashSet<String> set = new HashSet();
            set.add(roomId);
            IoCache.spaceClientMap.put(nameSpace, new SetWithLock(set));
        }
    }


    public static void removeRoom(String nameSpace, String roomId) {
        if (getRoomIds(nameSpace) != null) {
            IoCache.spaceRoomMap.get(nameSpace).remove(roomId);
        }
    }
}
